package com.example.cloud_microservice_user.service;


import com.example.cloud_microservice_user.model.AppUser;
import com.example.cloud_microservice_user.model.dto.AppUserDto;
import com.example.cloud_microservice_user.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class RestAppUserService {

    AppUserRepository appUserRepository;

    @Autowired
    public RestAppUserService(final AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    public Long create(final AppUserDto appUserDto) {
        AppUser appUser = new AppUser();
        appUser.setFirstName(appUserDto.getFirstName());
        appUser.setLastName(appUserDto.getLastName());
        appUser.setLogin(appUserDto.getLogin());

        appUser = appUserRepository.save(appUser);

        return appUser.getId();
    }


    public AppUserDto get(final Long id) {

        Optional<AppUser> userOptional = appUserRepository.findById(id);

        if (!userOptional.isPresent()){
            throw new EntityNotFoundException("user not found");
        }

        return mapFromAppUser(new AppUserDto(), userOptional.get());
    }


    public AppUserDto update(final Long id, final AppUserDto appUserDto){
        Optional<AppUser> userOptional = appUserRepository.findById(id);

        if(!userOptional.isPresent()) {
            throw new EntityNotFoundException("User not found");
        }
            AppUser appUser = mapFromAppUserDto(userOptional.get(), appUserDto);

            appUser = appUserRepository.save(appUser);

            return mapFromAppUser(appUserDto, appUser);
        }

    public void delete (final Long id){
        appUserRepository.deleteById(id);
    }




    private AppUserDto mapFromAppUser(AppUserDto appUserDto, AppUser appUser) {
        appUserDto.setFirstName(appUser.getFirstName());
        appUserDto.setLastName(appUser.getLastName());
        appUserDto.setLogin(appUser.getLogin());
        return appUserDto;
    }

    private AppUser mapFromAppUserDto(AppUser appUser, AppUserDto appUserDto) {
        appUser.setFirstName(appUserDto.getFirstName());
        appUser.setLastName(appUserDto.getLastName());
        appUser.setLogin(appUserDto.getLogin());
        return appUser;
    }

}
