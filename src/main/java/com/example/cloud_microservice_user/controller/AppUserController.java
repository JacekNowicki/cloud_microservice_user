package com.example.cloud_microservice_user.controller;

import com.example.cloud_microservice_user.model.dto.AppUserDto;
import com.example.cloud_microservice_user.service.RestAppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/user")
public class AppUserController {

    private RestAppUserService restAppUserService;

    @Autowired
    public AppUserController(final RestAppUserService restAppUserService) {
        this.restAppUserService = restAppUserService;
    }



    @PutMapping(path = "/add")
    @ResponseStatus(value = HttpStatus.CREATED)
    private Long createUser(@RequestBody AppUserDto appUserDto){
        return restAppUserService.create(appUserDto);
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    private AppUserDto getUser (Long id){
        return restAppUserService.get(id);
    }

    @PostMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    private AppUserDto updateUser (@RequestBody AppUserDto appUserDto,
                                   Long id){
        return restAppUserService.update(id, appUserDto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    private void deleteUser (Long id){
        restAppUserService.delete(id);
    }





}
